//
//  Request.cpp
//  SVR1
//
//  Created by Travail on 15/02/2019.
//  Copyright © 2019 Eyax. All rights reserved.
//

#include "Request.hpp"

namespace ey
{

Request::Request()
{
    m_requests["METHOD"] = "ZIA_NO_METHOD";
    m_requests["URI"] = "ZIA_NO_URI";
    m_requests["PROTOCOL-VERSION"] = "ZIA_NO_PROTOCOL_VERSION";
    m_requests["MESSAGE"] = "";
    m_requests["ERROR"] = "No Parsing Error";
}

std::unordered_map<std::string, std::string>& Request::getMap()
{
    return m_requests;
}

std::string& Request::operator[](const std::string& key)
{
    return m_requests[key];
}

RequestParser::RequestParser()
{
    
}

Request RequestParser::parse(const std::string& input)
{
    std::istringstream strm(input);
    std::string line;
    unsigned int linecount(0);
    unsigned int fn_tokencount(0);
    Request req;
    std::vector<std::string> tokens;
    bool isMessage(false);
    
    while (std::getline(strm, line)) //Line per line
    {
        if(!isMessage)
        {
            ++linecount;
            if(linecount == 1)
            {
                std::istringstream lnstm(line);
                std::string token;
                while (lnstm >> token) //Token per token
                {
                    ++fn_tokencount;
                    switch (fn_tokencount) {
                        case 1:
                            req["METHOD"] = token;
                            break;
                            
                        default:
                            tokens.push_back(token);
                            break;
                    }
                }
                
                std::string uri;
                for(auto i(0); i < tokens.size() - 1; ++i) //-1 because the last one is the protocol
                {
                    uri += tokens[i];
                    if(i != tokens.size()-1) uri += " ";
                }
                
                req["URI"] = uri;
                req["PROTOCOL-VERSION"] = tokens[tokens.size()-1];
                
                if(fn_tokencount < 3)
                    req["ERROR"] = "Invalid header";
            }
            else
            {
                bool isWhitespaceOnly(true);
                bool firstPart(true);
                std::pair<std::string, std::string> r;
                for(const auto& c : line)
                {
                    if(!isspace(c)) isWhitespaceOnly = false;
                    if(firstPart)
                    {
                        if(c == ':') firstPart = false;
                        else r.first += c;
                    }
                    else
                    {
                        r.second += c;
                    }
                }
                
                if(isWhitespaceOnly)
                {
                    isMessage = true;
                }
                else
                {
                    if(r.first[0] == ' ') r.first = r.first.substr(1, r.first.size());
                    if(r.second[0] == ' ') r.second = r.second.substr(1, r.second.size());
                }
                
                if(!r.first.empty() && !r.second.empty()) req[r.first] = r.second;
            }
        }
        else
        {
            req["MESSAGE"] += line + "\n";
        }
    }
    return req;
}

}
