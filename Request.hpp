//
//  Request.hpp
//  SVR1
//
//  Created by Travail on 15/02/2019.
//  Copyright © 2019 Eyax. All rights reserved.
//

#ifndef Request_hpp
#define Request_hpp

#include <string>
#include <memory>
#include <vector>
#include <unordered_map>
#include <sstream>

namespace ey
{

class Request
{
public:
    Request();
    
    std::unordered_map<std::string, std::string>& getMap();
    
    std::string& operator[](const std::string& key);
    
private:
    
    std::unordered_map<std::string, std::string> m_requests;
};

class RequestParser
{
public:
    RequestParser();
    
    Request parse(const std::string& input);
};
    
}

#endif /* Request_hpp */
