//
//  main.cpp
//  SVR1
//
//  Created by Travail on 14/02/2019.
//  Copyright © 2019 Eyax. All rights reserved.
//

#include <iostream>
#include <fstream>
#include "Request.hpp"

int main(int argc, const char * argv[])
{
    std::ifstream file;
    std::stringstream req_str;
    
    file.open("request.request");
    req_str << file.rdbuf();
    file.close();
    
    ey::Request req; //HTTP Request from user
    ey::RequestParser reqParser; //Parser
    
    req = reqParser.parse(req_str.str()); //We parse the string
    
    for(const auto& e : req.getMap())
    {
        std::cout << e.first << ": " << e.second << std::endl;
    }
    
    return 0;
}

//400 Bad Request
